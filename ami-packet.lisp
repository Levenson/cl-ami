;; Filename: object.lisp
;; Description: 
;; Author: User Alex
;; 
;; Maintainer: 
;; Created: Tue Sep 25 13:35:09 2012 (+0400)
;; Version: 
;; Last-Updated: Thu Dec 13 18:50:53 2012 (+0400)
;;           By: User Alex
;;     Update #: 618
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-ami)
(declaim #.*optimize-default*)

;; Small counter for ActionID field.
(let ((counter 0))
  (declare  #.*optimize-default*)
  (declare (type number counter))
  (defun counter-next () (incf counter))

  (defun counter-value () counter)

  (defun counter-reset () (setf counter 0)))

;;; AMI packets

(defclass ami-packet () 
  ((keys :type (or nil list)
	 :initarg :keys
	 :initform nil
	 :accessor ami-packet-keys
	 :documentation "Cons of keys i.e: <Key_Name>: <Value 1><CRLF>")
   (variables :type (or nil list)
	      :initarg :variables
	      :initform nil
	      :accessor ami-packet-variables
	      :documentation "Cons of variables i.e: <Variable_Name>=<Value 1><CRLF>")
   (lines :type (or nil list)
	  :initarg :lines
	  :initform nil
	  :accessor ami-packet-lines
	  :documentation "Packet content, line by line."))
  (:documentation "General object class for Asterisk AMI packets."))

(defclass ami-action (ami-packet)
  ()
  (:documentation "Response: the response sent by Asterisk to the last action sent by the
client."))

(defclass ami-response (ami-packet)
  ()
  (:documentation  "A packet sent by the connected client to Asterisk,
requesting a particular Action be performed. There are a finite (but
extendable) set of actions available to the client, determined by the
modules presently loaded in the Asterisk engine. Only one action may
be outstanding at a time. The Action packet contains the name of the
operation to be performed as well as all required parameters."))

(defclass ami-event (ami-packet)
  ()
  (:documentation "Data pertaining to an event generated from within the Asterisk
core or an extension module."))

(defmethod initialize-instance :after ((object ami-action) &rest initargs)
  (declare #.*optimize-default*)
  (declare (ignore initargs))
  (with-accessors ((keys ami-packet-keys)
		   (variables ami-packet-variables)
		   (lines ami-packet-lines)) object
    (log:sexp-debug "Building Object:" keys variables lines)
    (unless (get-key object "ActionID")
      (set-key object "ActionID" (princ-to-string (counter-next)))
      (log:trace "Action ID set to ~a" (counter-value)))))

(defmethod print-object ((object ami-packet) stream)
  (declare #.*optimize-default*)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "")))

(defun set-key-general (key value alist)
  (let ((pair (assoc key alist)))
    (if (consp pair)
	(rplacd pair value)
	(push (cons key value) alist)))
  alist)

(defmethod set-key ((object ami-packet) key value)
  "Set an additional key to the assoc list."
  (declare #.*optimize-default*)
  (with-accessors ((alist-keys ami-packet-keys)) object
    (setf alist-keys (set-key-general key value alist-keys))))

(defmethod get-key ((object ami-packet) key)
  (declare #.*optimize-default*)
  (with-accessors ((alist-keys ami-packet-keys)) object
    ;; (log:trace " GET KEY: ~S ~S" key (assoc key keys :test #'equal))
    (assoc key alist-keys :test #'equal)))

(defmethod set-variable ((object ami-packet) key value)
  (declare #.*optimize-default*)
  (with-accessors ((alist-variables ami-packet-variables)) object
    (setf alist-variables (set-key-general key value alist-variables))))

(defmethod get-variable ((object ami-packet) key)
  (declare #.*optimize-default*)
  (with-accessors ((variables ami-packet-variables)) object
    (assoc key variables)))

(defmethod successp ((object ami-packet))
  (declare #.*optimize-default*)
  (cond
    ((string= "Success" (cdr (get-key object "Response" ))) t)
    ((search "Follow" (cdr (get-key object "Response" ))) t)
    (t nil)))

(defun write-keys (list-keys stream)
  (dolist (pair list-keys)
    (format stream "~A:" (car pair))
    (etypecase (cdr pair)
      (keyword (format stream "~A:" (cdr pair)))
      (boolean (format stream "~:[off~;on~]~%" (cdr pair)))
      (t (format stream "~A~%" (cdr pair))))))

(defun write-variables (list-variables stream)
  (dolist (pair list-variables)
    (format stream "Variable: ~A=~S~%" (car pair) (cdr pair))))

(defmethod write-ami-packet ((object ami-packet) stream)
  (declare #.*optimize-default*)
  (with-accessors ((keys ami-packet-keys)
		   (variables ami-packet-variables)) object
    (write-keys keys stream)
    (write-variables variables stream)
    (stream-terpri stream)
    (stream-force-output stream)))

(defmethod print-object ((object ami-packet) (stream ami-stream))
  (declare #.*optimize-default*)
  (log:trace ">>> Printing Object ~S" object)
  (write-ami-packet object stream))

(defmethod ami-authorization ((stream ami-stream) username secret &key events)
  (declare #.*optimize-default*)
  (write (action "login" :username username :secret secret :events events) :stream stream))

(defmethod ami-authorization :around ((stream ami-stream) username secret &key events)
  (declare #.*optimize-default*)
  (declare (ignore username secret events))
  (let ((result (call-next-method))
	(auth-response (make-instance 'ami-response :keys (read-ami-block :stream stream))))
    (if (successp auth-response)
	(log:debug "Logged in by ~S" username)
	(error "AMI authorization faild!"))))

(defun read-ami-block (&optional &key (stream *ami-standard-stream*) (test #'string-empty-p))
  "Read one block of data and split its data by groups."
  (let ((lkeys) (lvariables) (llines) (end-result))
    (do ((line (read-line stream nil :eof)
	       (read-line stream nil :eof)))
	((or (funcall test line)
	     (eq line :eof))
	 (setf end-result line))
      (if (string-key-value-type-p line)
	  (let ((pair (string-split #\: line)))
	    (setf lkeys (acons (car pair) (cdr pair) lkeys)))
	  (push line llines)))
    (values lkeys lvariables (nreverse llines) end-result)))

(defun read-ami-block-response (response-line &optional (stream *ami-standard-stream*))
  "Reads response block based on Message tag off `Response:<Message>' response line"
  (cond
    ((search "Follow" response-line)
     (read-ami-block :stream stream :test (lambda (line) (string= "--END COMMAND--" line))))
    (t (read-ami-block :stream stream))))

(defun read-ami-block-event (&optional (stream *ami-standard-stream*))
  "Reads event block"
  (read-ami-block :stream stream))

(defun read-ami (&optional (stream *ami-standard-stream*))
  "Returns list of all `Responses', received through the ami socket."
  (let ((list-of-responses))  
    (do* ((line (read-line stream nil nil)
     		(read-line stream nil nil))
     	  ;; Buffer for pair
     	  (pair (string-split #\: line)
     		(string-split #\: line)))
     	 ((null line) :null)
      (cond
     	;; Skip empty strings
     	((string-empty-p line))
     	;; Some asterisk versions have a broken implementation for some
     	;; events/resposes (i.e: not even sending the "Event:" /
     	;; "Response:" keys,) sending a "follows" without a "complete",
     	;; sending "follow" instead of "follows", not even sending
     	;; "follows", etc.. a real nightmare).
	;; ************************* RESPONSE *************************
     	((search "Response" line)
     	 (multiple-value-bind (keys variables lines end-result) (read-ami-block-response line stream)
     	   (push (make-instance 'ami-response
				:keys (acons (car pair) (cdr pair) keys)
				:variables variables
				:lines lines)
		 list-of-responses)
	   ;; If and only if read-ami-block have finished its work
	   ;; with the :EOF then we shutdown the main loop too.
	   (when (eq end-result :eof)
	     (return nil))))
     	;; All event have to be send to *ami-event-stream*
	;; ************************* EVENT *************************
     	((search "Event" line)
     	 (multiple-value-bind (keys variables lines) (read-ami-block-event stream)
     	   (write (make-instance 'ami-event
				 :keys (acons (car pair) (cdr pair) keys)
				 :variables variables
				 :lines lines) :stream *ami-event-stream*)))))
    list-of-responses))

(defun get-response (ami-object &optional (stream *ami-standard-stream*))
  (let ((list-of-responses (read-ami stream)))
    (remove-if-not (lambda (response)
		     (string= (cdr (get-key ami-object "ActionID"))
			      (cdr (get-key response "ActionID")))) list-of-responses)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
