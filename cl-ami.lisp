;;;; -*- mode: lisp; -*-
;;;; Filename: ami.lisp
;;;; Description: 
;;;; Author: User Alex
;;;; 
;;;; Maintainer: 
;;;; Created: Mon Aug 27 14:18:05 2012 (+0400)
;;;; Version: 
;;;; Last-Updated: Thu Dec 13 00:43:47 2012 (+0400)
;;;;           By: User Alex
;;;;     Update #: 909
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-ami)
(declaim #.*optimize-default*)

(defparameter *event-base* nil)

(defparameter +max-buffer-size+ 16384)

(defparameter *actions-table* nil
  "Hash-table with actions-response pairs.")

(defparameter *ami-standard-stream* nil)
;; (defparameter *ami-standard-input* nil)
(defparameter *ami-event-stream* nil)

(log:config :debug :sane
	    ;; :properties "/home/alex/projects/cl-ami/log4cl.conf"
	    )

(defmacro with-ami-stream ((stream username secret &optional &key (events nil) (host #/IP/127.0.0.1) (port 5038)) &body body)
  `(call-with-ami-stream
    (lambda (,stream)
      (let ((*ami-standard-stream* ,stream)
	    (*ami-event-stream* *trace-output*))
	(ami-authorization ,stream ,username ,secret :events ,events)
	(progn
	  ,@body)))
    :host ,host :port ,port))

(defun call-with-ami-stream (function &key host port)
  (let ((stream))
    (unwind-protect
	 (multiple-value-prog1
	     (funcall function (setf stream (make-instance 'ami-stream :host host :port port)))) 
      (when stream (close stream)))))

(defun list-to-assoc (list)
  (if (evenp (length list))
      (nreverse (pairlis (remove-if-not #'keywordp list)
			 (remove-if #'keywordp list)))
      (error "Arguments are incorrect. Evenp list length needed")))

(defun action (&rest args)
  "Build an AMI Action"
  (let ((variables (rest (find-if #'(lambda (x)
				      (and (listp x)
					   (eq (car x) :variables))) args)))
	(keys (remove-if #'listp (push :action args))))
    (make-instance 'ami-action
		   :keys (list-to-assoc keys)
		   :variables (list-to-assoc variables))))

(defun variables (&rest args)
  (push :variables args))

;; (with-ami-stream (stream "skytel"  "zZnhrEhU4tCm" :events t :host #/IP/192.168.0.25)
;;   (get-response (write (action "command" :command "sip show peers"
;; 			       (variables :extid 3)) :stream stream)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Ami.lisp ends here
