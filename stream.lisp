;; Filename: stream.lisp
;; Description: AMI-Stream realization
;; Author: User Alex
;; 
;; Maintainer: 
;; Created: Thu Dec 13 10:14:54 2012 (+0400)
;; Version: 
;; Last-Updated: Thu Dec 13 10:20:31 2012 (+0400)
;;           By: User Alex
;;     Update #: 6
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(in-package :cl-ami)
(declaim #.*optimize-default*)

(defclass ami-stream (trivial-gray-stream-mixin
		      fundamental-character-input-stream
		      fundamental-character-output-stream)
  ((authorized :type boolean
	       :initform nil
	       :initarg :authorized
	       :accessor ami-stream-authorized)
   (host :type inet-address
	 :initarg :host 
	 :initform nil
	 :accessor ami-stream-host)
   (port :type number
	 :initarg :port
	 :initform nil
	 :accessor ami-stream-port)
   (version :type string
	    :initform ""
	    :accessor ami-stream-version)
   (socket :initarg :socket
	   :initform (make-socket :connect :active :address-family :internet
				  :type :stream
				  :ipv6 nil)
	   :accessor ami-stream-socket
	   :documentation "Raw socket of the AMI connection."))
  (:documentation "General AMI stream class"))

(defmethod initialize-instance :after ((stream ami-stream) &rest initargs)
  (declare #.*optimize-default*)
  (declare (ignore initargs))
  (with-accessors ((socket ami-stream-socket)
		   (host ami-stream-host)
		   (port ami-stream-port)
		   (version ami-stream-version)) stream
    (handler-case
	(connect socket
		 (lookup-hostname host) :port port :wait t)
      (socket-connection-refused-error ()
	(error "Connecion refused. Check if the port is opened~%"))
      (hangup ()
	(error "Uncaught hangup. Server closed connection on write!!~%" ))
      (end-of-file ()
	(error "Uncaught end-of-file. Server closed connection on read!!~%")))
    (setf version (read-line stream))
    (when (socket-connected-p socket)
      (log:trace "Remote connection from ~a:~a to ~a:~a"
		 (remote-host socket) (remote-port socket)
		 (local-host socket) (local-port socket)))))

(defmethod close ((stream ami-stream) &key abort)
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (close socket :abort abort)))

(defmethod stream-element-type ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (stream-element-type socket)))

;;; Stream Write

(defmethod stream-write-char ((stream ami-stream) char)
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (log:trace ">>> Char ~S" char)
    (write-char char socket)))

(defmethod stream-write-string ((stream ami-stream) string &optional start end)
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (log:trace ">>> String ~S" string)
    (stream-write-string socket string start end)))

(defmethod stream-terpri ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (log:trace ">>> EOL")
    (stream-write-char socket #\Return)
    (stream-write-char socket #\Newline)))

(defmethod stream-force-output ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (log:trace ">>> Force Output")
    (force-output socket)))

;;; Stream Read

(defmethod stream-read-char ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (let ((character (stream-read-char socket)))
      (log:trace "<<< Read Char ~S" character)
      (cond
	((char= #\Newline character)
	 ;;--- FIXME: Sometimes socket doesn't ready yet, and we will
	 ;;--- receive nil by stream-read-char-no-hang, so let's wait
	 ;;--- for awhile.
	 (sleep 0.001)
	 (let ((ch (stream-read-char-no-hang stream)))
	   (if (or (null ch)
		   (eq ch :eof))
	       :eof
	       (progn
		 (stream-unread-char stream ch)
		 character))))
	((char= #\Return character)
	 (stream-read-char stream))
	(t character)))))

(defmethod stream-read-char-no-hang ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (let ((character (stream-read-char-no-hang socket)))
      (log:trace "<<< Read Char No Hang ~S" character)
      character)))

(defmethod stream-unread-char ((stream ami-stream) character)
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (log:trace "<<< Unread Char ~S" character)
    (stream-unread-char socket character)))

(defmethod stream-peek-char ((stream ami-stream))
  (declare #.*optimize-default*)
  (with-accessors ((socket ami-stream-socket)) stream
    (let ((character (stream-peek-char socket)))
      (log:trace "<<< Peek Character ~S to" character)
      character)))

(defmethod stream-read-line ((stream ami-stream))
  (declare #.*optimize-default*)
  (let* ((len 80)
	 (string (make-string len))
	 (index 0))
    (loop
       (let ((ch (stream-read-char stream)))
	 (cond ((eq ch :eof)
		(log:trace "<<< String EOF ~S" (subseq string 0 index))
		(return (values	(subseq string 0 index) t)))
	       ((char= ch #\Newline)
		(log:trace "<<< String ~S" (subseq string 0 index))
		(return (values	(subseq string 0 index) nil)))
	       (t
		(when (= index len)
		  (setq len (* len 2))
		  (let ((new (make-string len)))
		    (replace new string)
		    (setq string new)))
		(setf (schar string index) ch)
		(incf index)))))))



;;; Event Stream

;;--- TODO: ami-event-stream have to be a broadcast stream for all
;;--- connected clients, which asked as to inform them about any kind
;;--- of event through AMI.

(defclass ami-event-stream (trivial-gray-stream-mixin
			    fundamental-character-input-stream
			    fundamental-character-output-stream)()
  (:documentation "An *ami-event-input* is a STREAM that actually
represent AMI socket for receiving an Event packets."))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; stream.lisp ends here
