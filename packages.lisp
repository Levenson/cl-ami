;; Filename: packages.lisp
;; Description: 
;; Author: User Alex
;; 
;; Maintainer: 
;; Created: Tue Sep 11 18:35:45 2012 (+0400)
;; Version: 
;; Last-Updated: Sat Dec  1 11:56:13 2012 (+0400)
;;           By: User Alex
;;     Update #: 9
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)

(defpackage :cl-ami
  (:nicknames :asterisk-ami)
  (:use :cl :iolib :trivial-gray-streams))

(in-package :cl-ami)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *optimize-default* '(optimize (speed 0) (debug 3) (safety 0) (compilation-speed 0))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; packages.lisp ends here
