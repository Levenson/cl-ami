;; Filename: cl-ami.asd
;; Description: 
;; Author: User Alex
;; 
;; Maintainer: 
;; Created: Tue Sep 11 18:42:53 2012 (+0400)
;; Version: 
;; Last-Updated: Thu Dec 13 18:48:52 2012 (+0400)
;;           By: User Alex
;;     Update #: 17
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)

(defpackage :cl-ami-asd
  (:use :cl :asdf))

(in-package :cl-ami-asd)

(defsystem :cl-ami
  :version "0.1"
  :serial t
  :depends-on (:iolib :flexi-streams :log4cl :split-sequence)
  :components ((:file "packages")
	       (:file "utils")
	       (:file "stream")
	       (:file "ami-packet")
	       (:file "cl-ami")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; cl-ami.asd ends here
