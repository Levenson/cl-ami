;; Filename: utils.lisp
;; Description: 
;; Author: User Alex
;; 
;; Maintainer: 
;; Created: Tue Sep 11 19:50:08 2012 (+0400)
;; Version: 
;; Last-Updated: Tue Dec 11 16:08:13 2012 (+0400)
;;           By: User Alex
;;     Update #: 25
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-ami)
(declaim #.*optimize-default*)

(defparameter +char-bag+ '(#\Tab #\Space))

(defun string-position (character sequence)
  (position character sequence :test #'char=))

(defun string-key-value-type-p (sequence &optional (char-delimiter #\:))
  (let ((position (string-position char-delimiter sequence)))
    (if (and position
	     (null (find #\Space (subseq sequence 0 position))))
	t nil)))

(defun string-split (char-delimiter sequence)
  (reduce #'cons
	  (split-sequence:split-sequence char-delimiter sequence :test #'char= )
	  :key (lambda (x) (string-trim +char-bag+ x))))

(defun string-empty-p (string)
  (string= "" string))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; utils.lisp ends here
